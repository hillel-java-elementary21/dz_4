package org.example;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class ArrayObjectListTest {

    @Test
    public void testAppendItems() {
        ObjectList<String> cities = new ArrayObjectList();
        cities.add("Dnipro");
        cities.add("Kiev");
        cities.add("Kharkiv");
        cities.add("Lviv");
        assertEquals(4, cities.size());
        assertEquals("Dnipro", cities.get(0));
        assertEquals("Kiev", cities.get(1));
        assertEquals("Kharkiv", cities.get(2));
        assertEquals("Lviv", cities.get(3));
    }

    @Test
    public void testChangeItems() {
        ObjectList<String> cities = new ArrayObjectList();
        cities.add("Dnipro");
        cities.add("Kiev");
        cities.add("Kharkiv");
        cities.set(1, "Pripyat");
        assertEquals(3, cities.size());
        assertEquals("Dnipro", cities.get(0));
        assertEquals("Pripyat", cities.get(1));
        assertEquals("Kharkiv", cities.get(2));
    }

    @Test
    public void testOutOfBounds() {
        ObjectList<String> cities = new ArrayObjectList<>();
        cities.add("Dnipro");
        cities.add("Kiev");
        assertThrows(IndexOutOfBoundsException.class, () -> cities.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> cities.get(2));
        assertThrows(IndexOutOfBoundsException.class, () -> cities.get(4));
        assertDoesNotThrow(() -> cities.get(0));
        assertDoesNotThrow(() -> cities.get(1));
    }

    @Test
    public void testAddAll() {
        ObjectList<String> cities = new ArrayObjectList();
        ObjectList<String> cities2 = new ArrayObjectList();
        cities.add("Dnipro");
        cities.add("Kiev");
        cities.add("Kharkiv");
        cities.add("Lviv");
        cities2.add("Odessa");
        cities2.add("Orikhov");
        cities2.add("Ostrog");
        cities2.add("Pyriatyn");
        cities.addAll(cities2);
        assertEquals(8, cities.size());
        assertEquals("Dnipro", cities.get(0));
        assertEquals("Kiev", cities.get(1));
        assertEquals("Kharkiv", cities.get(2));
        assertEquals("Lviv", cities.get(3));
        assertEquals("Odessa", cities.get(4));
        assertEquals("Orikhov", cities.get(5));
        assertEquals("Ostrog", cities.get(6));
        assertEquals("Pyriatyn", cities.get(7));
    }

    @Test
    public void testIterator() {
        int index = 0;
        ObjectList<String> cities = new ArrayObjectList<>();
        cities.add("Dnipro");
        cities.add("Kiev");
        cities.add("Kharkiv");
        cities.add("Lviv");
        cities.add("Odessa");
        cities.add("Orikhov");
        cities.add("Ostrog");
        cities.add("Pyriatyn");
        Iterator<String> iterator = cities.iterator();
        while (iterator.hasNext()) {
            assertEquals(cities.get(index), iterator.next());
            index++;
        }
    }

    @Test
    public void testReversIterator() {
        ObjectList<Integer> numbers = new ArrayObjectList<>();
        numbers.add(-1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(-5);
        numbers.add(6);
        numbers.add(7);
        numbers.add(8);
        Iterator<Integer> iterator = numbers.reversIterator();
        int index = 7;
        while (iterator.hasNext()) {
            assertEquals(numbers.get(index), iterator.next());
            index--;
        }
    }

    @Test
    public void testReduce() {
        ObjectList<Integer> list = new ArrayObjectList<>();
        list.add(1);
        list.add(2);
        list.add(4);
        list.add(5);
        Integer sum = list.reduce(0, (c, p) -> p + c);
        Integer mult = list.reduce(1, (c, p) -> p * c);
        assertEquals(12, sum);
        assertEquals(40, mult);

        ObjectList<Integer> list2 = new ArrayObjectList<>();
        list2.add(1);
        Integer sum2 = list2.reduce(0, (c, p) -> p + c);
        Integer mult2 = list2.reduce(1, (c, p) -> p * c);
        assertEquals(1, sum2);
        assertEquals(1, mult2);

        ObjectList<Integer> list3 = new ArrayObjectList<>();
        list3.add(-1);
        list3.add(-2);
        list3.add(-4);
        list3.add(-5);
        Integer sum3 = list3.reduce(0, (c, p) -> p + c);
        Integer mult3 = list3.reduce(1, (c, p) -> p * c);
        assertEquals(-12, sum3);
        assertEquals(40, mult3);
    }

    @Test
    public void testEveryAndSome() {
        ObjectList<Integer> numbers = new ArrayObjectList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(-3);
        numbers.add(4);
        numbers.add(5);
        assertFalse(numbers.every(e -> e > 0));
        assertTrue(numbers.some(e -> e > 0));

        ObjectList<Integer> numbers2 = new ArrayObjectList<>();
        numbers2.add(-1);
        assertFalse(numbers2.every(e -> e > 0));
        assertFalse(numbers2.some(e -> e > 0));
        assertTrue(numbers2.every(e -> e < 0));
        assertTrue(numbers2.some(e -> e < 0));

        ObjectList<Integer> numbers3 = new ArrayObjectList<>();
        numbers3.add(-1);
        numbers3.add(-1);
        numbers3.add(-1);
        assertTrue(numbers3.every(e -> e == -1));
        assertTrue(numbers3.some(e -> e == -1));
    }
}
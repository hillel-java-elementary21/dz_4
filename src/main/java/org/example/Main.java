package org.example;

import java.util.Iterator;


public class Main {
    public static void main(String[] args) {
        ObjectList<String> cities = new ArrayObjectList<>();
        cities.add("Dnipro");
        cities.add("Kiev");
        cities.add("Kharkiv");
        cities.add("Lviv");
        cities.add("Odessa");
        cities.add("Orikhov");
        Iterator<String> iterator = cities.iterator();
        Iterator<String> reversIterator = cities.reversIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("--------");
        while (reversIterator.hasNext()) {
            System.out.println(reversIterator.next());
        }
    }
}

package org.example;

import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

interface ObjectList<T> {
    T get(int index);

    void set(int index, T value);

    void add(T value);

    void addAll(ObjectList<T> list);

    int size();

    boolean every(Predicate<T> predicate);

    boolean some(Predicate<T> predicate);

    <R> R reduce(R initValue, BiFunction<T, R, R> biFunction);

    Iterator<T> iterator();

    Iterator<T> reversIterator();
}

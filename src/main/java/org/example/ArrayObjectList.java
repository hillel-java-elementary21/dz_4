package org.example;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

class ArrayObjectList<T> implements ObjectList<T> {
    Object[] objects;
    int size = 0;
    private final static int DEFAULT_CAPACITY = 8;
    private final static double SCALE_FACTOR = 1.5;

    public ArrayObjectList() {
        this(DEFAULT_CAPACITY);
    }

    public ArrayObjectList(int capacity) {
        this.objects = new Object[capacity];
    }

    public T get(int index) {
        checkIndex(index);
        return (T) objects[index];
    }

    public void set(int index, Object value) {
        checkIndex(index);
        objects[index] = value;
    }

    public void add(Object value) {
        if (size >= objects.length) expend();
        objects[size++] = value;
    }

    @Override
    public void addAll(ObjectList list) {
        for (int i = 0; i < list.size(); i++) {
            add(list.get(i));
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean every(Predicate<T> predicate) {
        boolean result = false;
        for (int i = 0; i < this.size(); i++) {
            if (predicate.test(this.get(i))) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean some(Predicate<T> predicate) {
        boolean result = false;
        for (int i = 0; i < this.size(); i++) {
            if (predicate.test(this.get(i))) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }

    public class MyIterator implements Iterator<T> {
        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < objects.length;
        }

        @Override
        public T next() {
            return (T) objects[index++];
        }
    }

    public Iterator<T> reversIterator() {
        return new Iterator<T>() {
            private int index = objects.length - 1;

            public boolean hasNext() {
                return index >= 0;
            }

            public T next() {
                return (T) objects[index--];
            }
        };
    }

    @Override
    public <R> R reduce(R initValue, BiFunction<T, R, R> biFunction) {
        R result = initValue;
        for (int i = 0; i < this.size; i++) {
            result = biFunction.apply(get(i), initValue);
            initValue = result;
        }
        return result;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }

    private void expend() {
        int newCapacity = (int) (objects.length * SCALE_FACTOR + 1);
        objects = Arrays.copyOf(objects, newCapacity);
    }
}




